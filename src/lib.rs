use std::error::Error;
use chrono::{NaiveDateTime, DateTime, Utc};
use std::str::FromStr;
use std::fmt;
use miniseed::ms_record;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

pub fn record_hash(rec: &ms_record) -> u64 {
    let mut hasher = DefaultHasher::new();
    rec.id().hash(&mut hasher);
    rec.start().hash(&mut hasher);
    rec.end().hash(&mut hasher);
    rec.npts().hash(&mut hasher);
    return hasher.finish();
}

fn parse_time_forgivingly(dstr: &str) -> Option<DateTime<Utc>> {
    if let Ok(time) = dstr.parse::<DateTime<Utc>>() {
        Some(time)
    }
    else if let Ok(time) = NaiveDateTime::parse_from_str(dstr, "%Y-%m-%d %H:%M:%S") {
        Some(DateTime::<Utc>::from_utc(time, Utc))
    }
    else {
        None
    }
}

#[derive(Clone)]
pub struct TimeInterval {
    start: DateTime<Utc>,
    end: DateTime<Utc>,
}

impl TimeInterval {
    fn _parse(s: &str) -> Option<Self> {
        let ts: Vec<&str> = s.split("~").collect();
        if ts.len() < 2 {
            return None;
        }
        let t0 = parse_time_forgivingly(ts[0])?;
        let t1 = parse_time_forgivingly(ts[1])?;
        Some(TimeInterval { start: t0, end: t1 })
    }

    pub fn overlaps(&self, other: &Self) -> bool {
        return self.start <= other.end && other.start <= self.end;
    }
}

#[derive(Debug, Clone)]
pub struct TimeIntervalParseError;
impl fmt::Display for TimeIntervalParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", "Invalid time interval format.")
    }
}
impl Error for TimeIntervalParseError {}

impl FromStr for TimeInterval {
    type Err = TimeIntervalParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match TimeInterval::_parse(s) {
            Some(x) => Ok(x),
            None => Err(TimeIntervalParseError),
        }
    }
}

pub struct RecordFilter<'a> {
    filterer: Box<dyn Fn(&ms_record) -> bool + 'a>,
}
impl<'a> RecordFilter<'a> {
    pub fn new<F: Fn(&ms_record) -> bool + 'a>(f: F) -> Self {
        return Self {
            filterer: Box::new(f)
        }
    }

    pub fn matches(&self, r: &ms_record) -> bool {
        return (*(self.filterer))(r);
    }

    pub fn pass_all() -> Self {
        Self::new(|_| true)
    }

    pub fn in_interval(ti: TimeInterval) -> Self {
        Self::new(move |record: &ms_record| {
            ti.overlaps(&TimeInterval {
                start: record.start(),
                end: record.end(),
            })
        })
    }
}

