/*
   Copyright 2021 Geoscience Australia

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
extern crate miniseed;
extern crate clap;
extern crate chrono;

use std::collections::HashMap;

use miniseed::{ms_input, ms_output, ms_record};
use clap::{Clap, crate_version};

use chrono::{NaiveDateTime, DateTime, Utc};
use std::str::FromStr;

#[derive(Clap)]
#[clap(version=crate_version!(), author="Anthony Carapetis <anthony.carapetis@ga.gov.au>")]
struct Opts {
    #[clap(short, long)]
    output_file: String,
    #[clap(short='E', long, about="Sort by record end time instead of start time")]
    end_time: bool,

    #[clap(short='t', long, about="Time interval to trim to (same format as scmssort: YY-mm-dd HH:MM:SS~YY-mm-dd HH:MM:SS)")]
    trim: Option<TimeInterval>,

    input_files: Vec<String>,
}

struct TimeInterval {
    start: DateTime<Utc>,
    end: DateTime<Utc>,
}

#[derive(Debug, Clone)]
struct TimeIntervalParseError;

fn parse_time_forgivingly(dstr: &str) -> Option<DateTime<Utc>> {
    if let Ok(time) = dstr.parse::<DateTime<Utc>>() {
        Some(time)
    }
    else if let Ok(time) = NaiveDateTime::parse_from_str(dstr, "%Y-%m-%d %H:%M:%S") {
        Some(DateTime::<Utc>::from_utc(time, Utc))
    }
    else {
        None
    }
}

impl ToString for TimeIntervalParseError {
    fn to_string(&self) -> String {
        "Invalid time interval format.".to_owned()
    }
}

impl TimeInterval {
    fn _parse(s: &str) -> Option<Self> {
        let ts: Vec<&str> = s.split("~").collect();
        if ts.len() < 2 {
            return None;
        }
        let t0 = parse_time_forgivingly(ts[0])?;
        let t1 = parse_time_forgivingly(ts[1])?;
        Some(TimeInterval { start: t0, end: t1 })
    }

    fn overlaps(&self, other: &Self) -> bool {
        return self.start <= other.end && other.start <= self.end;
    }
}

impl FromStr for TimeInterval {
    type Err = TimeIntervalParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match TimeInterval::_parse(s) {
            Some(x) => Ok(x),
            None => Err(TimeIntervalParseError),
        }
    }
}

struct Input {
    head: ms_record,
    stream: ms_input,
}

type Inputs = HashMap<usize, Input>;

fn init_stream(mut stream: ms_input) -> Option<Input> {
    let first = stream.next()?;
    Some(Input { stream: stream, head: first })
}

#[derive(Debug)]
enum MSMergeError {
    UnsortedInput(String),
}

impl ToString for MSMergeError {
    fn to_string(&self) -> String {
        match self {
            MSMergeError::UnsortedInput(filename) => format!("Unsorted input file {}", filename)
        }
    }
}

fn read_record<F,V>(inputs: &mut Inputs, ix: &usize, sortkey: F) -> Result<(), MSMergeError>
    where F: Fn(&ms_record) -> V, V: Ord
{
    let mut input = inputs.get_mut(ix).unwrap();
    if let Some(rec) = input.stream.next() {
        if sortkey(&rec) < sortkey(&input.head) {
            return Err(MSMergeError::UnsortedInput(input.stream.filename().to_string()));
        }
        input.head = rec;
    } else {
        inputs.remove(ix);
    }
    return Ok(())
}

struct RecordFilter<'a> {
    filterer: Box<dyn Fn(&ms_record) -> bool + 'a>,
}
impl<'a> RecordFilter<'a> {
    fn new<F: Fn(&ms_record) -> bool + 'a>(f: F) -> Self {
        return Self {
            filterer: Box::new(f)
        }
    }

    fn matches(&self, r: &ms_record) -> bool {
        return (*(self.filterer))(r);
    }

    fn pass_all() -> Self { Self::new(|_| true) }
}



fn merge<F,V>(input_files: Vec<ms_input>, output: &mut ms_output,
                  sortkey: F, interval: Option<TimeInterval>) -> Result<(), MSMergeError>
    where F: Fn(&ms_record) -> V, V: Ord
{
    let pair_key = |(_, r): &(&usize, &Input)| sortkey(&r.head);

    let mut inputs: Inputs = input_files.into_iter()
        .filter_map(init_stream)
        .enumerate()
        .collect();

    let filter =
        if let Some(span) = interval {
            RecordFilter::new(move |record: &ms_record| {
                span.overlaps(&TimeInterval {
                    start: record.start(),
                    end: record.end(),
                })
            })
        } else {
            RecordFilter::pass_all()
        };

    while !inputs.is_empty() {
        // Pop off the next record in the requested order
        let pair = inputs.iter().min_by_key(pair_key);
        if let Some((name, input)) = pair {
            if filter.matches(&input.head) {
                output.write(&input.head);
            }
            let n2 = name.clone(); // needs to be on its own line to give up the borrow of inputs
            read_record(&mut inputs, &n2, &sortkey)?;
        }
    }
    return Ok(());
}

fn main() -> Result<(), MSMergeError> {
    let opts: Opts = Opts::parse();

    let inputs = opts.input_files.into_iter().map(ms_input::open);
    let mut output = ms_output::open(opts.output_file).unwrap();
    let sortkey = if opts.end_time { ms_record::end }
                  else             { ms_record::start };

    merge(inputs.collect(), &mut output, sortkey, opts.trim)
}
