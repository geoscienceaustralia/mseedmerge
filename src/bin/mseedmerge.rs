/*
   Copyright 2021 Geoscience Australia

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
extern crate miniseed;
extern crate clap;
extern crate chrono;

use std::collections::HashMap;
use std::collections::HashSet;

use mseedmerge::{TimeInterval, RecordFilter, record_hash};

use miniseed::{ms_input, ms_output, ms_record};
use clap::{Clap, crate_version};

#[derive(Clap)]
#[clap(version=crate_version!(), author="Anthony Carapetis <anthony.carapetis@ga.gov.au>")]
struct Opts {
    #[clap(short, long)]
    output_file: String,
    #[clap(short='E', long, about="Sort by record end time instead of start time")]
    end_time: bool,

    #[clap(short='t', long, about="Time interval to trim to (same format as scmssort: YY-mm-dd HH:MM:SS~YY-mm-dd HH:MM:SS)")]
    trim: Option<TimeInterval>,

    #[clap(short='d', long, about="Remove records with identical waveform stream IDs, time intervals and sample rates")]
    deduplicate: bool,

    input_files: Vec<String>,
}

struct Input {
    head: ms_record,
    stream: ms_input,
}

type Inputs = HashMap<usize, Input>;

fn init_stream(mut stream: ms_input) -> Option<Input> {
    let first = stream.next()?;
    Some(Input { stream: stream, head: first })
}

#[derive(Debug)]
enum MSMergeError {
    UnsortedInput(String),
}

impl ToString for MSMergeError {
    fn to_string(&self) -> String {
        match self {
            MSMergeError::UnsortedInput(filename) => format!("Unsorted input file {}", filename)
        }
    }
}

fn read_record<F,V>(inputs: &mut Inputs, ix: &usize, sortkey: F) -> Result<(), MSMergeError>
    where F: Fn(&ms_record) -> V, V: Ord
{
    let mut input = inputs.get_mut(ix).unwrap();
    if let Some(rec) = input.stream.next() {
        if sortkey(&rec) < sortkey(&input.head) {
            return Err(MSMergeError::UnsortedInput(input.stream.filename().to_string()));
        }
        input.head = rec;
    } else {
        inputs.remove(ix);
    }
    return Ok(())
}

fn merge<F,V>(input_files: Vec<ms_input>,
              output: &mut ms_output,
              sortkey: F,
              interval: Option<TimeInterval>,
              deduplicate: bool,
              ) -> Result<(), MSMergeError>
    where F: Fn(&ms_record) -> V, V: Ord
{
    let pair_key = |(_, r): &(&usize, &Input)| sortkey(&r.head);

    let mut inputs: Inputs = input_files.into_iter()
        .filter_map(init_stream)
        .enumerate()
        .collect();

    let filter =
        if let Some(span) = interval {
            RecordFilter::in_interval(span)
        } else {
            RecordFilter::pass_all()
        };

    let mut seen = HashSet::new();

    while !inputs.is_empty() {
        // Pop off the next record in the requested order
        let pair = inputs.iter().min_by_key(pair_key);
        if let Some((name, input)) = pair {
            if filter.matches(&input.head) {
                if deduplicate {
                    let hash = record_hash(&input.head);
                    if !seen.contains(&hash) {
                        output.write(&input.head);
                        seen.insert(hash);
                    }
                } else {
                    output.write(&input.head);
                }
            }
            let n2 = name.clone(); // needs to be on its own line to give up the borrow of inputs
            read_record(&mut inputs, &n2, &sortkey)?;
        }
    }
    return Ok(());
}

fn main() -> Result<(), MSMergeError> {
    let opts: Opts = Opts::parse();

    let inputs = opts.input_files.into_iter().map(ms_input::open);
    let mut output = ms_output::open(opts.output_file).unwrap();
    let sortkey = if opts.end_time { ms_record::end }
                  else             { ms_record::start };

    merge(inputs.collect(), &mut output, sortkey, opts.trim, opts.deduplicate)
}
