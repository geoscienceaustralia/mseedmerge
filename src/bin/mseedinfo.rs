/*
   Copyright 2021 Geoscience Australia

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

extern crate miniseed;
extern crate clap;
extern crate chrono;
extern crate serde_json;

use std::collections::HashSet;

use chrono::{NaiveDateTime, DateTime, Utc};
use miniseed::{ms_input, ms_output, ms_record};
use clap::{Clap, crate_version};
use serde_json::json;

#[derive(Clap)]
#[clap(version=crate_version!(), author="Anthony Carapetis <anthony.carapetis@ga.gov.au>")]
struct Opts {
    input_files: Vec<String>,
}

fn main() {
    let opts: Opts = Opts::parse();

    let mut trids = HashSet::new();
    let mut quality = HashSet::new();

    let mut start: Option<DateTime<Utc>> = None;
    let mut end: Option<DateTime<Utc>> = None;
    let mut count = 0;
    for fname in opts.input_files {
        let input = ms_input::open(fname);
        for r in input {
            count += 1;

            let mut trid = format!("{}.{}.{}.{}", r.network(), r.station(), r.location(), r.channel());
            trid.retain(|c| !c.is_whitespace());
            trids.insert(trid);
            quality.insert(r.dataquality());
            let et = r.end();
            let st = r.start();
            if let Some(old) = start {
                if st < old {
                    start = Some(st)
                }
            } else {
                start = Some(st)
            }
            if let Some(old) = end {
                if et > old {
                    end = Some(et)
                }
            } else {
                end = Some(et)
            }
        }
    }

    let mut result = json!({
        "trids": trids,
        "quality": quality,
        "record_count": count,
    });
    if let Some(s) = start {
        result["start_time"] = json!(s.format("%Y-%m-%dT%H:%M:%SZ").to_string());
    }
    if let Some(e) = end {
        result["end_time"] = json!(e.format("%Y-%m-%dT%H:%M:%SZ").to_string());
    }
    println!("{}", result);

    /*
    if let Some(s) = start {
        if let Some(e) = end {
            println!("{} {}", s, e);
        }
    }
    let mut first = false;
    for trid in trids {
        if !first {
            print!(", ");
        }
        print!("{}", trid);
        first = false;
    }
    println!();
    */
}
