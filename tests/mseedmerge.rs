extern crate miniseed;
extern crate tempdir;

use miniseed::{ms_input, ms_record};
use tempdir::TempDir;
use std::process::Command;
use std::error::Error;

fn is_sorted_by_end_time<T: Iterator<Item=ms_record>>(mut f: T) -> bool {
    match f.next() {
        None => true,
        Some(r0) => {
            let mut t0 = r0.end();
            for r in f {
                let t = r.end();
                if t < t0 {
                    return false;
                }
                t0 = t;
            }
            return true;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    /// Test executable produces sorted results from sorted data
    #[test]
    fn test_sort_by_end_time () -> Result<(), Box<dyn Error>> {
        let outdir = TempDir::new("mseedmerge_test")?;
        let outpathbuf = outdir.path().join("merged.mseed");
        let outpath = outpathbuf.to_str().ok_or("Error building output path")?;

        let script = format!("cargo run --bin mseedmerge -- -E tests/data/sorted/*.mseed -o {}", outpath);
        Command::new("sh").arg("-c").arg(script).status()?;

        let result = ms_input::open(outpath);
        assert!(is_sorted_by_end_time(result));
        Ok(())
    }

    /// Test executable produces an error when run on unsorted data
    #[test]
    fn test_fails_on_unsorted_inputs () -> Result<(), Box<dyn Error>> {
        let script = "cargo run --bin mseedmerge -- -E tests/data/unsorted/*.mseed -o /dev/null";
        let status = Command::new("sh").arg("-c").arg(script).status()?;
        assert!(!status.success());
        Ok(())
    }
}
