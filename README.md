# mseedmerge

This is a command-line utility for merging time-sorted MiniSEED files on disk.

**Each input file must already be time-sorted to get correct results.**

## Installation

A mostly statically-linked binary is available on the public SKIP. It should be
compatible with any recent Linux distribution (e.g. CentOS 7+). To install to
e.g. `~/bin`:

    curl https://cdn.eatws.net/skip/products/mseedmerge -o ~/bin/mseedmerge
    chmod +x ~/bin/mseedmerge

## Usage

If you have a directory `waveforms` full of time-sorted miniseed files, e.g.
one per station, then to merge them into a single time-sorted miniseed file:

    mseedmerge waveforms/*.mseed -o merged.mseed

If the records are sorted by end time instead of start time, use the `-E` flag.
